# Labels

Labels for the phones at the stand. [svgsagoma](https://github.com/etuardu/svgsagoma) is used as the templating engine.

## Setup

Init and pull the submodule for the [svgsagoma](https://github.com/etuardu/svgsagoma) templating engine:

```
git submodule init
git submodule update
```

## Data

Put the data for the phones into `data.csv`

## Generate

Generate the labels:

```
make
```